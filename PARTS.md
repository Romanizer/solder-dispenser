# List of Parts / Datasheets

These are ALL the parts needed to assemble 1x solder paste dispenser

## PCB BOM

- Stepper Driver (Trinamic)
- MCU
- Passives
- USB Type-C Port
- PSP Joystick
- Pressure Sensor(s)

## Mechanical Parts

- [Fisnar Syringe body 30ml - 8001004](https://www.tme.eu/en/details/fis-barqx-30r/barrels-cartridges/fisnar/8001004/)
- [Fisnar Plunger 30/55ml -  8001009](https://www.tme.eu/en/details/fis-pisqx-sf-30_55/barrels-cartridges/fisnar/8001009/)
- Syringe Needles of various sizes
- Syringe Needle with rubber ring
- Stepper Motor
- Threaded Rod
- Threaded Inserts
