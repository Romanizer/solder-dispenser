# solder dispenser

Open Source Solder Paste Dispenser (and more).
Can dispense very small quantities of solder paste, flux, adhesieves...
Also usable for part placement, with an angled rubber tip needle.

Input:
* PSP Joystick
* FSR402 force sensor ?

Using a Trinamic stepper driver to actuate the syringe.

The dispenser uses FISNAR syringes and plungers, with a standard coupling for tips.

# Parts

* Nema 14 Stepper [14HM08](https://www.omc-stepperonline.com/nema-14-bipolar-0-9deg-5ncm-7-08oz-in-0-5a-5v-35x35x20mm-4-wires-14hm08-0504s) or [14HS13](https://www.omc-stepperonline.com/nema-14-bipolar-1-8deg-18ncm-25-5oz-in-0-8a-5-4v-35x35x34mm-4-wires-14hs13-0804s)

## RP2040 Project
Located in `rp2040` and `rp2040-try`.

First, add the rp2040 architecture to your targets:
```sh
rustup target add thumbv6m-none-eabi
```

Install cargo-generate and flip-link, elf2uf2-rs and probe-rs:
```sh
cargo install flip-link
cargo install cargo-generate
cargo install elf2uf2-rs --locked
# or if using a SWD debug probe:
cargo install probe-rs -features=cli --locked
```

Initialize a new embedded rp2040 project by running:
```sh
cargo generate gh:rp-rs/rp2040-project-template -n cool-rp2040-project
```
